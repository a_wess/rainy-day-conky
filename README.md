# Rainy Day Weather

A conky mod which uses darksky API.

Requires an API key from Dark Sky: https://darksky.net/
replace REPLACEWITHAPIKEY in the configuration file with your API and choose coordinates. 